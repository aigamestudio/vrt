﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SaveManagerBehaviour : MonoBehaviour {
    public static SaveManagerBehaviour Instance;

    [Header("Save to Cloud")]
    [SerializeField] Text _notificationText;
    [SerializeField] string updateURL = "https://segarbugarindo.com/nis/Update.php";
    [SerializeField] string updateSuccessStr = "Update success";

    bool isSaving = false;

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    IEnumerator SaveToCloudCoroutine()
    {
        isSaving = true;
        _notificationText.gameObject.SetActive(true);
        while (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if(_notificationText.gameObject.activeSelf)
            {
                _notificationText.text = "Couldn't save to database.\nPlease check your internet connection.";
                yield return new WaitForSeconds(3.0f);
                _notificationText.gameObject.SetActive(false);
            }
            yield return null;
        }

        _notificationText.gameObject.SetActive(true);
        _notificationText.text = "Saving to database ...";
        yield return new WaitForSeconds(1.0f);

        WWWForm updateForm = new WWWForm();
        updateForm.AddField("usernamePost", PlayerPrefs.GetString("name"));
        updateForm.AddField("recentLevelPost", PlayerPrefs.GetString("lastLevelName"));
        updateForm.AddField("totalScorePost", PlayerPrefs.GetInt("currentPoint"));
        WWW www = new WWW(updateURL, updateForm);
        float timer = 0;
        float timeOut = 10;
        bool failed = false;
        while (!www.isDone)
        {
            if (timer > timeOut) { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (failed || !string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            _notificationText.text = "Unable to connect to database";
            yield return new WaitForSeconds(1.0f);
            _notificationText.gameObject.SetActive(false);
            yield break;
        }

        if (www.text == updateSuccessStr) _notificationText.text = updateSuccessStr;
        else _notificationText.text = "Unable to connect to database";

        yield return new WaitForSeconds(1.0f);
        _notificationText.gameObject.SetActive(false);
        isSaving = false;
    }

    public void SaveToCloud()
    {
        if (!isSaving)
        {
            StartCoroutine(SaveToCloudCoroutine());
        }
    }
}

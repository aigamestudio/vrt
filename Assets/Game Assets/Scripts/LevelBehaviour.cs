﻿using UnityEngine;
using System.Collections;

/**
*LevelBehaviour
*This class will be stored the information regarding the current level
*such as the object of the waypoint and both of the lift
**/
public class LevelBehaviour : MonoBehaviour {
	[HideInInspector] PlayerBehaviour _player;
	[HideInInspector] WaypointBehaviour[] _waypoints;
	[HideInInspector] MiniGameBehaviour[] _miniGames;

	[HideInInspector] GameObject _liftIntro;
	[HideInInspector] GameObject _liftExit;

	[SerializeField] string levelName = "Prologue";
	[SerializeField] string nextLevelName = "credit";
	[SerializeField] float levelInitialTimeLeft = 0.0f;
	[SerializeField] float levelFadeInSpeed = 1.0f;
	[SerializeField] WaypointBehaviour startWaypoint;
	[SerializeField] float liftIntroCheckRadius = 0.1f;
	[SerializeField] float liftExitRepeatPosY = -10.0f;
	[SerializeField] float liftExitMaxPosY = -30.0f;

	void Awake() {
		_player = FindObjectOfType<PlayerBehaviour>();
		_liftIntro = transform.FindChild("Meshes").FindChild("Lift Intro").gameObject;
		_liftExit = transform.FindChild("Meshes").FindChild("Lift Exit").gameObject;

		_waypoints = new WaypointBehaviour[transform.FindChild("Waypoints").childCount];
		for(int i = 0; i < _waypoints.Length; i++) {
			_waypoints[i] = transform.FindChild("Waypoints").GetChild(i).GetComponent<WaypointBehaviour>();
		}

		_miniGames = new MiniGameBehaviour[transform.FindChild("Mini Games").childCount];
		for(int i = 0; i<_miniGames.Length; i++) {
			_miniGames[i] = transform.FindChild("Mini Games").GetChild(i).GetComponent<MiniGameBehaviour>();
		}
	}

	void OnEnable() {
		for(int i = 0; i < _waypoints.Length; i++) {
			_waypoints[i].id = i;
			_waypoints[i].gameObject.SetActive(false);
		}
	}

	public void HandleLiftIntro() {
		_player.transform.SetParent(_liftIntro.transform, true);
		_liftIntro.transform.Translate(Vector3.down * Time.deltaTime);

		if(!_liftIntro.GetComponent<AudioSource>().isPlaying) _liftIntro.GetComponent<AudioSource>().Play();
	}

	public void HandleLiftExit() {
		_player.transform.SetParent(_liftExit.transform, true);
		_liftExit.transform.Translate(Vector3.down * Time.deltaTime);
		if(_liftExit.transform.position.y < liftExitMaxPosY) {
			float x = _liftExit.transform.position.x;
			float z = _liftExit.transform.position.z;
			_liftExit.transform.position = new Vector3(x, liftExitRepeatPosY, z);
		}

		if(!_liftExit.GetComponent<AudioSource>().isPlaying) _liftExit.GetComponent<AudioSource>().Play();
	}

	public bool IsLiftIntroIsDoneMoving() {
		return Vector3.Distance(_liftIntro.transform.position, startWaypoint.transform.position) < liftIntroCheckRadius;
	}

	public void ResetLevel() {
		for(int i = 0; i < _waypoints.Length; i++) {
			_waypoints[i].gameObject.SetActive(false);
		}

		for(int i=0; i<_miniGames.Length; i++) {
			_miniGames[i].SetInteractable(false);
		}
	}

	public void StartLevel() {
		_liftIntro.GetComponent<AudioSource>().Stop();

		_player.transform.SetParent(transform.parent);
		_player.TeleportToWaypoint(startWaypoint);
		startWaypoint.SetWaypointInteractableActive(_player.transform.position);
	}

	public string GetLevelName() { return levelName; }
	public string GetNextLevelName() { return nextLevelName; }
	public float GetLevelInitialTimeLeft() { return levelInitialTimeLeft; }
	public float GetLevelFadeInSpeed() { return levelFadeInSpeed; }
}
	
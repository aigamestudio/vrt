﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/**
*CognitiveMiniGameBehaviour
*Class that will control's every cognitive mini game behaviour
*This class will generates the question and the right answer by it self
**/
public class CognitiveMiniGameBehaviour : MiniGameBehaviour {
	[HideInInspector] CognitiveMiniGameAnswerBehaviour _question;
	[HideInInspector] CognitiveMiniGameAnswerBehaviour[] _answers;

	[SerializeField] int maxUnlockChance = 2;
	[SerializeField] protected float questionTime = 3.0f;
	[SerializeField] float exitTime = 3.0f;

	int phase = 0;
	int unlockChance = 0;
	int rightAnswer = -1;
	
	protected new virtual void Awake() {
        base.Awake();

		_answers = new CognitiveMiniGameAnswerBehaviour[_hologram.FindChild("Answers").childCount];
		_question = _hologram.FindChild("Question").GetComponent<CognitiveMiniGameAnswerBehaviour>();
		for(int i = 0; i < _answers.Length; i++) {
			_answers[i] = _hologram.FindChild("Answers").GetChild(i).GetComponent<CognitiveMiniGameAnswerBehaviour>();
		}
	}

	protected virtual void OnEnable() {
		SetupActiveHologramObjects(false, false);
		SetupIsOpenForAllAnswerObjects(false);
	}

	protected void SetupActiveHologramObjects(bool questionObject, bool answerObjects) {
		_hologram.FindChild("Meshes").gameObject.SetActive(questionObject || answerObjects);
		_question.gameObject.SetActive(questionObject);
		for(int i = 0; i < _answers.Length; i++) {
			_answers[i].gameObject.SetActive(answerObjects);
		}
	}

	private void SetupQuestionAndAnswer() {
		List<int> possibleAnswerId = new List<int>();
		for(int i=0; i<CognitiveMiniGameAnswerBehaviour.MAX_ANSWER_ID; i++) {
			possibleAnswerId.Add(i);
		}

		rightAnswer = Random.Range(0, _answers.Length);
		_question.answerId = Random.Range(0, CognitiveMiniGameAnswerBehaviour.MAX_ANSWER_ID);
		_answers[rightAnswer].answerId = _question.answerId;
		possibleAnswerId.Remove(_question.answerId);
		for(int i=0; i<_answers.Length; i++) {
			if(i != rightAnswer) {
				_answers[i].answerId = possibleAnswerId[Random.Range(0, possibleAnswerId.Count)];
				possibleAnswerId.Remove(_answers[i].answerId);
			}
		}
	}

	protected void SetupAnswerPhase() {
		_gameManager.NextState();
		SetupActiveHologramObjects(true, true);
		SetupIsOpenForAllAnswerObjects(false);
	}

	private void SetupIsOpenForAllAnswerObjects(bool cond) {
		for(int i=0; i<_answers.Length; i++) {
			_answers[i].SetIsOpen(cond);
		}
	}

	private void EndExitPhase() {
		_gameManager.CheckWinState();
		SetupActiveHologramObjects(false, false);
		SetupIsOpenForAllAnswerObjects(false);
	}

	public void SetupCognitiveMiniGame(bool isNoPhysicalMiniGame) {
		unlockChance = maxUnlockChance;
		SetupQuestionAndAnswer();
		SetupActiveHologramObjects(false, true);
		SetupIsOpenForAllAnswerObjects(true);
		if(isNoPhysicalMiniGame) Invoke("SetupAnswerPhase", questionTime);
	}

	public void ProcessAnswerId(int proposedAnswerId) {
		bool isTrueAnswer = proposedAnswerId == rightAnswer;
		_gameManager.currentPoint += (isTrueAnswer) ? truePoint : falsePoint;
		_gameManager.trueAnswerCount += (isTrueAnswer) ? 1 : 0;
		_gameManager.falseAnswerCount += (!isTrueAnswer) ? 1 : 0;
		if(isTrueAnswer) {
			isUnlocked = true;
			_animator.SetBool("isUnlocked", isUnlocked);
			_gameManager.NextState();
			Invoke("EndExitPhase", exitTime);
		}
		else {
			unlockChance--;
			if(unlockChance <= 0) {
				_gameManager.NextState();
				_animator.SetTrigger("lose");
				Invoke("EndExitPhase", exitTime);
			} else {
				_animator.SetTrigger("wrongAnswer");
			}
		}
	}

    public override Vector3 GenerateHologramCenter()
    {
        Bounds bounds = new Bounds(_question.transform.position, Vector3.zero);
        for(int i=0; i<_answers.Length; i++)
        {
            bounds.Encapsulate(_answers[i].transform.position);
        }
        return bounds.center;
    }

	public override void ActivatePlayerPointer() {
		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring && !isUnlocked) {
			_player.MoveToMiniGame(this);
			_player.GetComponent<Animator>().SetBool("pointer", false);
			_gameManager.CognitiveMiniGameState();
		}
	}

	public override void SetPlayerPointer(bool cond) {
		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring && !isUnlocked) {
			_player.GetComponent<Animator>().SetBool("pointer", cond);
		}
	}

	public int GetUnlockChance() { return unlockChance; }
}

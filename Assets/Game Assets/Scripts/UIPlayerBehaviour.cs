﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/**
*UIPlayerBehaviour
*This class is control's everything that appear on the player screen in run time
*such as the text that appear when player is lose or win
**/
public class UIPlayerBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] LevelBehaviour _level;
    [HideInInspector] Animator _animator;
	[HideInInspector] Text _pointText;
    [HideInInspector] Transform _leftRight;

	float outLoseTime = 3.0f;
	float outWinTime = 3.0f;

	bool isPaused = false;
	bool hasWin = false;
	bool hasLose = false;

	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
		_animator = GetComponent<Animator>();
		_pointText = transform.FindChild("Pause").FindChild("Point Text").GetComponent<Text>();
        _leftRight = transform.FindChild("Left & Right");
	}

	void OnEnable() {
		_animator.SetFloat("fadeInMultiplier", _level.GetLevelFadeInSpeed());
	}

	void Update() {
		HandlePauseUI();
        HandleLeftRightUI();
		switch(_gameManager.GetState()) {
			case GameManagerBehaviour.GameState.Win: HandleWinUI(); break;
			case GameManagerBehaviour.GameState.Lose: HandleLoseUI(); break;
		}
	}

	private void HandleWinUI() {
		if(!hasWin) {
			hasWin = true;
			Invoke("OutWinAnimation", outWinTime);
		}
	}

	private void HandleLoseUI() {
		if(!hasLose) {
			hasLose = true;
			_animator.SetBool("isLose", true);
			Invoke("OutLoseAnimation", outLoseTime);
		}
	}

	[HideInInspector] float lerpProgress = 0.0f;
	private void HandlePauseUI() {
		_pointText.text = _gameManager.currentPoint + " points";

		_animator.SetBool("isPaused", isPaused);
		transform.SetParent((isPaused) ? Camera.main.transform.parent : Camera.main.transform);
		if(!isPaused) {
			lerpProgress = Mathf.Min(1.0f, lerpProgress + 0.01f);
			transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, 0, 1), lerpProgress);
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.Euler(0, 0, 0), lerpProgress);
		}

		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring) {
			if(Input.GetKeyDown(KeyCode.Escape)) {
				isPaused = !isPaused;
				lerpProgress = 0.0f;
			}
		} else {
			isPaused = false;
        }
	}

    private void HandleLeftRightUI()
    {
        bool isInState = _gameManager.GetState() == GameManagerBehaviour.GameState.Exploring ||
                            _gameManager.GetState() == GameManagerBehaviour.GameState.Moving ||
                            _gameManager.GetState() == GameManagerBehaviour.GameState.Intro;
        _leftRight.gameObject.SetActive(isInState);
    }

	public void ContinueButton() {
		isPaused = false;
	}

	private void OutLoseAnimation() {
		_animator.SetTrigger("outLose");
	}

	private void OutWinAnimation() {
		_animator.SetTrigger("outWin");
	}

	public void ChangeLevel() {
		SceneManager.LoadScene(_level.GetNextLevelName());
	}


	public void GameOverLevel() {
		_gameManager.RestartLevel();
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
		
	public void RestartLevel() {
		_gameManager.RestartLevel();
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void Quit() {
		Application.Quit();
	}
}

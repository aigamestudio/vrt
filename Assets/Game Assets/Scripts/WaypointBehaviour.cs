﻿using UnityEngine;
using System.Collections;

/**
*WaypointBehaviour
*Class that control's the waypoint behaviour
*and decide which are the way point that approachable by player
*/
public class WaypointBehaviour : VRButtonBehaviour {
	[HideInInspector] public int id { get; set; }
	[HideInInspector] GameManagerBehaviour _gameManager;

	[SerializeField] WaypointBehaviour[] _nextWaypoint;
	[SerializeField] MiniGameBehaviour[] _availableMiniGames;

	[SerializeField] bool haveLiftExit = false;

	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_player = FindObjectOfType<PlayerBehaviour>();
	}

	public override void ActivatePlayerPointer() {
		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring) {
			_player.MoveToWaypoint(this);
			_player.GetComponent<Animator>().SetBool("pointer", false);
			_gameManager.NextState();
		}
	}

	public override void SetPlayerPointer(bool cond) {
		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring) {
			_player.GetComponent<Animator>().SetBool("pointer", cond);
		}
	}

	public void SetWaypointInteractableActive(Vector3 positionLookAt) {
		for(int i = 0; i < _nextWaypoint.Length; i++) {
			_nextWaypoint[i].gameObject.SetActive(true);
			_nextWaypoint[i].transform.LookAt(positionLookAt);
		}

		for(int i=0; i < _availableMiniGames.Length; i++) {
			_availableMiniGames[i].SetInteractable(!_availableMiniGames[i].GetIsUnlocked());
		}
	}

	public bool GetHaveLiftExit() { return haveLiftExit; }
}

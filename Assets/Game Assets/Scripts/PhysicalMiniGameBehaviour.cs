﻿using UnityEngine;
using System.Collections;

/**
*PhysicalMiniGameBehaviour
*Class that will control's every physical mini game behaviour
*This class will generates the question and the right answer by it self
**/
public class PhysicalMiniGameBehaviour : CognitiveMiniGameBehaviour {
	[HideInInspector] Animator _modelAnimator;
	[HideInInspector] TextMesh _activityText;
    [HideInInspector] Transform _tutorialPosture;

    [SerializeField] AudioClip stepIncreaseClip;
    [SerializeField] AudioClip winClip;
    [SerializeField] float toleranceTime = 0.1f;
	[SerializeField] PhysicalMiniGameActivityBehaviour[] _activities;

	int activityCount = 0;

	[System.Serializable]
	public class PhysicalMiniGameActivityBehaviour : System.Object {
		public string name = "";
		public string animationName = "";
        public int needMove = 5;
		public AudioClip storyAudio;
        public PhysicalMiniGameActivityStepBehaviour[] physicalMiniGameActivitySteps;
	}

    [System.Serializable]
    public class PhysicalMiniGameActivityStepBehaviour : System.Object
    {
        [Tooltip("-2 = Kanan, 2 = Kiri")]
        [Range(-2.0f, 2.0f)] public float xAccelormeterMin = 0.0f;

        [Tooltip("-2 = Atas, 2 = Bawah")]
        [Range(-2.0f, 2.0f)] public float yAccelormeterMin = 0.0f;

        [Tooltip("-2 = Menjauh, 2 = Mendekat")]
        [Range(-2.0f, 2.0f)] public float zAccelormeterMin = 0.0f;

        public int GetDirectionCountMin()
        {
            int count = 0;
            count = ((xAccelormeterMin < 0) ? 1 : 0) + ((xAccelormeterMin > 0) ? 1 : 0) +
                ((yAccelormeterMin < 0) ? 1 : 0) + ((yAccelormeterMin > 0) ? 1 : 0) +
                ((zAccelormeterMin > 0) ? 1 : 0) + ((zAccelormeterMin < 0) ? 1 : 0);
            return count;
        }
    }

	protected override void Awake () {
		base.Awake();
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_player = FindObjectOfType<PlayerBehaviour>();
		_animator = GetComponent<Animator>();
		_hologram = transform.FindChild("Hologram");
		_modelAnimator = _hologram.FindChild("Model").GetComponent<Animator>();
		_activityText = _hologram.FindChild("Activity Text").GetComponent<TextMesh>();
        _tutorialPosture = _hologram.FindChild("Tutorial");
	}

	protected override void OnEnable() {
		base.OnEnable();
		SetupActiveHologramObjects(false);
        _tutorialPosture.gameObject.SetActive(false);

        currentMoveCount = 0;
        stepCheck = 0;
    }

	private void SetupActiveHologramObjectsPhysicalMiniGameOff() { SetupActiveHologramObjects(false); }
	private void SetupActiveHologramObjects(bool cond) {
		_hologram.FindChild("Quad").gameObject.SetActive(cond);
		_hologram.FindChild("Activity Text").gameObject.SetActive(cond);
		_hologram.FindChild("Model").gameObject.SetActive(cond);
	}

	private void DoPhysicalActivity() {
		SetupActiveHologramObjects(false, false);
		SetupActiveHologramObjects(true);
		_modelAnimator.SetTrigger(_activities[activityCount].animationName);
		GetComponent<AudioSource>().PlayOneShot(_activities[activityCount].storyAudio);
        StartCoroutine(TutorialActivity());
    }

    private IEnumerator TutorialActivity()
    {
        _activityText.gameObject.SetActive(false);
        _tutorialPosture.gameObject.SetActive(true);
        while(true)
        {
            if (Input.GetMouseButton(0)) break;
            yield return new WaitForEndOfFrame();
        }
        _activityText.gameObject.SetActive(true);
        _tutorialPosture.gameObject.SetActive(false);
        StartCoroutine(CheckNextActivityAvailable());
    }

    [HideInInspector] int currentMoveCount = 0;
    [HideInInspector] int stepCheck = 0;
    private IEnumerator CheckNextActivityAvailable()
    {
        while (currentMoveCount < _activities[activityCount].needMove)
        {
            int checkCount = 0;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].xAccelormeterMin < 0.0f &&
                Input.acceleration.x <= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].xAccelormeterMin) checkCount++;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].xAccelormeterMin > 0.0f &&
                Input.acceleration.x >= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].xAccelormeterMin) checkCount++;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].yAccelormeterMin < 0.0f &&
                Input.acceleration.y <= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].yAccelormeterMin) checkCount++;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].yAccelormeterMin > 0.0f &&
                Input.acceleration.y >= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].yAccelormeterMin) checkCount++;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].zAccelormeterMin < 0.0f &&
                Input.acceleration.z <= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].zAccelormeterMin) checkCount++;
            if (_activities[activityCount].physicalMiniGameActivitySteps[stepCheck].zAccelormeterMin > 0.0f &&
                Input.acceleration.z >= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].zAccelormeterMin) checkCount++;

            if (checkCount >= _activities[activityCount].physicalMiniGameActivitySteps[stepCheck].GetDirectionCountMin())
            {
                stepCheck++;
                yield return new WaitForSeconds(toleranceTime);
            }

            if (stepCheck >= _activities[activityCount].physicalMiniGameActivitySteps.Length)
            {
                stepCheck = 0;
                currentMoveCount++;
                GetComponent<AudioSource>().PlayOneShot(stepIncreaseClip);
            }

            _activityText.text = _activities[activityCount].name + "\n" + currentMoveCount;
            yield return new WaitForEndOfFrame();
        }

        if (activityCount + 1 < _activities.Length)
        {
            activityCount++;
            currentMoveCount = 0;
            DoPhysicalActivity();
        }
        else
        {
            GetComponent<AudioSource>().PlayOneShot(winClip);
            SetupAnswerPhase();
            SetupActiveHologramObjectsPhysicalMiniGameOff();
        }
    }

	public void SetupPhysicalMiniGame() {
		activityCount = 0;
		SetupCognitiveMiniGame(false);
		Invoke("DoPhysicalActivity", questionTime);
		_gameManager.Invoke("NextState", questionTime);
	}

	public override void ActivatePlayerPointer() {
		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring && !isUnlocked) {
			_player.MoveToMiniGame(this);
			_player.GetComponent<Animator>().SetBool("pointer", false);
			_gameManager.PhysicalMiniGameState();
		}
	}
}

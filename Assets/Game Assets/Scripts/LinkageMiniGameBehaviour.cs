﻿using UnityEngine;
using System.Collections;

public class LinkageMiniGameBehaviour : MiniGameBehaviour {

	[HideInInspector] TextMesh _questionText;
	[HideInInspector] VRButtonBehaviour[] _answers;

	[SerializeField] string succeedStr = "Succeed!";

	protected new virtual void Awake() {
        base.Awake();

		_questionText = _hologram.FindChild("Question Text").GetComponent<TextMesh>();
		_answers = new VRButtonBehaviour[_hologram.FindChild("Answers").childCount];
		for(int i = 0; i < _answers.Length; i++) {
			_answers[i] = _hologram.FindChild("Answers").GetChild(i).GetComponent<VRButtonBehaviour>();
		}
	}

	public void TrueButton() {
		isUnlocked = true;
		_animator.SetBool("isUnlocked", isUnlocked);
		_questionText.text = succeedStr;
		_gameManager.currentPoint += truePoint;
		_gameManager.trueAnswerCount++;
		for(int i = 0; i < _answers.Length; i++) {
			_answers[i].gameObject.SetActive(false);
		}
	}

	public void WrongButton() {
		_gameManager.currentPoint = Mathf.Max(0, _gameManager.currentPoint - falsePoint);
		_gameManager.falseAnswerCount++;
		_animator.SetTrigger("wrongAnswer");
	}

    public override Vector3 GenerateHologramCenter()
    {
        Bounds bounds = new Bounds();
        bounds.Encapsulate(_questionText.transform.position);
        for(int i=0; i<_answers.Length; i++)
        {
            bounds.Encapsulate(_answers[i].transform.position);
        }

        return bounds.center;
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

/**
*UITimeMonitorBehaviour
*This class will handle's the UI of the timer of the game
*and used by The black monitor in the game to point out the remaining time
**/
public class UITimeMonitorBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] GameTimeManagerBehaviour _gameTimeManager;
	[HideInInspector] LevelBehaviour _level;
	[HideInInspector] Text _timeLeftText;
	[HideInInspector] Text _pointText;

	[SerializeField] float alarmStart = 10.0f;
	[SerializeField] string gameOverStr = "Game Over!";
	int lastAlarm;

	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_gameTimeManager = _gameManager.GetComponent<GameTimeManagerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
		_timeLeftText = transform.FindChild("Time Left Text").GetComponent<Text>();
		_pointText = transform.FindChild("Point Text").GetComponent<Text>();
	}

	void OnEnable() {
		lastAlarm = (int)alarmStart;
	}

	void Update() {
		_pointText.text = _gameManager.currentPoint + " points";

		if(_level.GetLevelInitialTimeLeft() > 0.0f) {
			float timeLeft = _gameTimeManager.GetTimeLeft();
			_timeLeftText.text = (timeLeft > 0.0f) ? ConvertFloatToTime(timeLeft) + "s" : gameOverStr;
			if(timeLeft <= lastAlarm) {
				lastAlarm--;
				if(timeLeft > 0.0f) GetComponent<Animator>().SetTrigger("alert");
				else GetComponent<Animator>().SetTrigger("over");
			}
		}
	}

	private string ConvertFloatToTime(float timeFloat) {
		var ms = (int)((timeFloat - (int)timeFloat) * 10);
		var ss = Convert.ToInt32(timeFloat % 60).ToString("00");
		var mm = (Math.Floor(timeFloat / 60) % 60).ToString("00");
		return mm + ":" + ss + ":" + ms;
	}
		
	public void PlaySound(AudioClip _sfx) {
		GetComponent<AudioSource>().PlayOneShot(_sfx);
	}
}

﻿using UnityEngine;
using System.Collections;

/**
*PlayerBehaviour
*Class that control's the player behaviour
*like deciding which waypoint will be approached by player
**/
public class PlayerBehaviour : MonoBehaviour {
	[SerializeField] float speed = 3.0f;
    [SerializeField] float rotateSpeed = 90.0f;
    [SerializeField] float checkRadius = 0.1f;
	[SerializeField] float checkInteractableRadius = 0.5f;

	[HideInInspector] public WaypointBehaviour currentWaypoint { get; set; }
	[HideInInspector] public MiniGameBehaviour currentMiniGame { get; set; }

    int currentRotation = 0;

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "narrator")
        {
            AudioSource audio = collider.gameObject.GetComponent<AudioSource>();
            if (!audio.isPlaying)
            {
                audio.Play();
                Destroy(collider.gameObject, audio.clip.length);
            }
        }
    }

    [HideInInspector] bool playerRotatorCoroutineIsRunning = false;
    private IEnumerator HandlePlayerRotation()
    {
        playerRotatorCoroutineIsRunning = true;
        while (playerRotatorCoroutineIsRunning)
        {
            Camera.main.transform.localRotation = Quaternion.Lerp(Camera.main.transform.localRotation, Quaternion.Euler(new Vector3(0.0f, currentRotation * 90.0f)), rotateSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public void TurnLeft() {
        currentRotation = (int)Mathf.Repeat(currentRotation - 1, 4);
        if(!playerRotatorCoroutineIsRunning) StartCoroutine(HandlePlayerRotation());
    }

    public void TurnRight() {
        currentRotation = (int)Mathf.Repeat(currentRotation + 1, 4);
        if (!playerRotatorCoroutineIsRunning) StartCoroutine(HandlePlayerRotation());
    }

    public void InteractInReticle() {
		RaycastHit hitInfo;
		Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo);
		VRButtonBehaviour vrButton = hitInfo.transform.GetComponent<VRButtonBehaviour>();
		if(vrButton) {
			vrButton.ActivatePlayerPointer();
			return;
		}
	}

	public void MoveToWaypoint(WaypointBehaviour waypoint) {
		currentWaypoint = waypoint;
		transform.position = Vector3.MoveTowards(transform.position, waypoint.transform.position, speed * Time.deltaTime);
		if(!GetComponent<AudioSource>().isPlaying) GetComponent<AudioSource>().Play();
	}

	public void TeleportToWaypoint(WaypointBehaviour waypoint) {
		currentWaypoint = waypoint;
		transform.position = waypoint.transform.position;
	}

	public bool IsInWaypoint(WaypointBehaviour waypoint) {
		return Vector3.Distance(transform.position, waypoint.transform.position) < checkRadius;
	}

	public void MoveToMiniGame(MiniGameBehaviour miniGame) {
		currentMiniGame = miniGame;
		if(Vector3.Distance(transform.position, miniGame.transform.position) > checkInteractableRadius) {
			transform.position = Vector3.MoveTowards(transform.position, miniGame.transform.position, speed * Time.deltaTime);
			if(!GetComponent<AudioSource>().isPlaying) GetComponent<AudioSource>().Play();
		}
        
        if(playerRotatorCoroutineIsRunning) playerRotatorCoroutineIsRunning = false;
        Camera.main.transform.LookAt(miniGame.GenerateHologramCenter());
	}
}

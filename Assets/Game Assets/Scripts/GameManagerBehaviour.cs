﻿using UnityEngine;
using System.Collections;

/**
*GameManagerBehaviour
*This class will be stored every state that will be checked in the run time of the game
*by other object
**/
public class GameManagerBehaviour : MonoBehaviour {
	public enum GameState { Intro, Setup, Exploring, Moving,
							CognitiveMiniGameSetup, CognitiveMiniGameQuestion, CognitiveMiniGameAnswer, CognitiveMiniGameExit,
							PhysicalMiniGameSetup, PhysicalMiniGameQuestion, PhysicalMiniGamePlay, PhysicalMiniGameAnswer, PhysicalMiniGameExit,
							CheckWin, Win, Lose }
	GameState gameState = GameState.Intro;

	[HideInInspector] PlayerBehaviour _player;
	[HideInInspector] LevelBehaviour _level;

	public int currentPoint = 0;
	public int trueAnswerCount = 0;
	public int falseAnswerCount = 0;

	[SerializeField] int restartPenaltyPoint = -50;
	bool isHasSaved = false;

	void Awake() {
		_player = FindObjectOfType<PlayerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
	}

	void OnEnable() {
		currentPoint = PlayerPrefs.GetInt("currentPoint", 0);
		PlayerPrefs.SetString("lastLevelName", UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
        if(SaveManagerBehaviour.Instance) SaveManagerBehaviour.Instance.SaveToCloud();
    }

	void Update() {
		switch(gameState) {
			case GameState.Intro:
				_level.HandleLiftIntro();
				if(_level.IsLiftIntroIsDoneMoving()) {
					_level.StartLevel();
					gameState = GameState.Setup;
				}
				break;
			case GameState.Setup:
				_player.currentWaypoint.SetWaypointInteractableActive(_player.transform.position);
				gameState = GameState.Exploring;
				break;
			case GameState.Moving:
				_player.MoveToWaypoint(_player.currentWaypoint);
				if(_player.IsInWaypoint(_player.currentWaypoint)) {
					gameState = GameState.CheckWin;
				}
				break;
			case GameState.CognitiveMiniGameSetup:
				_level.ResetLevel();
				((CognitiveMiniGameBehaviour)_player.currentMiniGame).SetupCognitiveMiniGame(true);
				gameState = GameState.CognitiveMiniGameQuestion;
				break;
			case GameState.CognitiveMiniGameQuestion: break;
			case GameState.CognitiveMiniGameAnswer: break;
			case GameState.CognitiveMiniGameExit: break;
			case GameState.PhysicalMiniGameSetup: 
				_level.ResetLevel();
				((PhysicalMiniGameBehaviour)_player.currentMiniGame).SetupPhysicalMiniGame();
				gameState = GameState.PhysicalMiniGameQuestion;
				break;
			case GameState.PhysicalMiniGameQuestion: break;
			case GameState.PhysicalMiniGamePlay: break;
			case GameState.PhysicalMiniGameAnswer: break;
			case GameState.PhysicalMiniGameExit: break;
			case GameState.CheckWin:
				_level.ResetLevel();
				bool isWin = _player.currentWaypoint.GetHaveLiftExit();
				gameState = (isWin) ? GameState.Win : GameState.Setup;
				break;
			case GameState.Win:
				_level.HandleLiftExit();
				if(!isHasSaved) { isHasSaved = true; PlayerPrefs.SetInt("currentPoint", currentPoint); }
				break;
			case GameState.Lose:
				_level.ResetLevel();
				//if(PlayerPrefs.HasKey("lastLevelName") && PlayerPrefs.HasKey("currentPoint")) PlayerPrefs.DeleteAll(); // kenapa diapus??? top 10 scientist cant answer
				break;
		}
	}

	public void NextState() {
		gameState = gameState + 1;
	}

	public void CheckWinState() {
		gameState = GameState.CheckWin;
	}

	public void LoseState() {
		gameState = GameState.Lose;
	}

	public void CognitiveMiniGameState() {
		gameState = GameState.CognitiveMiniGameSetup;
	}

	public void PhysicalMiniGameState() {
		gameState = GameState.PhysicalMiniGameSetup;
	}

	public void RestartLevel() {
		currentPoint = Mathf.Max(0, currentPoint + restartPenaltyPoint);
		PlayerPrefs.SetInt("currentPoint", currentPoint);
	}

	public GameState GetState() { return gameState; }
}

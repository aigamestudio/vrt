﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

/**
*	UI Intro Screen Behaviour
*	Only used in Intro Screen
*	Needed for load save file
*/

public class UIIntroScreenBehaviour : MonoBehaviour
{
    [SerializeField] Text _notificationText;

    [SerializeField] Transform _usernamePanel;
    [SerializeField] InputField _usernameInputField;
    [SerializeField] Button _usernameButton;
    [SerializeField] string checkUsernameURL = "localhost/mr_agent_nis/CheckUsername.php";
    [SerializeField] string checkUsernameSuccessStr = "Username found";

    [SerializeField] Transform _registerPanel;
    [SerializeField] InputField _registerPasswordInputField;
    [SerializeField] InputField _registerReEnterPasswordInputField;
    [SerializeField] Button _registerButton;
    [SerializeField] string registerURL = "localhost/mr_agent_nis/Register.php";
    [SerializeField] string registerSuccessStr = "Register success";
    string registeredUsername = "";

    [SerializeField] Transform _loginPanel;
    [SerializeField] InputField _loginPasswordInputField;
    [SerializeField] Button _loginButton;
    [SerializeField] string loginURL = "localhost/mr_agent_nis/Login.php";
    [SerializeField] string loginUnsuccessfulStr = "Password incorrect";

    [SerializeField] string defaultNextLevelName = "prologue";

    string nextLevelName;

    void OnEnable() {
        // cek punya cache apa engga
        if (!CheckSaveCache()) {
            // buka account panel
            Time.timeScale = 0;
            transform.FindChild("Account Panel").gameObject.SetActive(true);
            StartCoroutine(CheckInternetConnection());
        } else
        {
            nextLevelName = PlayerPrefs.GetString("lastLevelName", defaultNextLevelName);
        }
    }
    
    void Update()
    {
        if(_usernamePanel.gameObject.activeSelf)
            _usernameButton.interactable = _usernameInputField.text.Length > 0;
        if(_registerPanel.gameObject.activeSelf)
            _registerButton.interactable = _registerPasswordInputField.text.Length > 0 && _registerReEnterPasswordInputField.text == _registerPasswordInputField.text;
        if(_loginPanel.gameObject.activeSelf)
            _loginButton.interactable = _loginPasswordInputField.text.Length > 0;
    }

    bool CheckSaveCache()
    {
        return PlayerPrefs.HasKey("name") && PlayerPrefs.HasKey("lastLevelName") && PlayerPrefs.HasKey("currentPoint");
    }

    IEnumerator CheckInternetConnection()
    {
        _notificationText.gameObject.SetActive(true);
        while (Application.internetReachability == NetworkReachability.NotReachable)
        {
            _notificationText.text = "Check your internet connection and please try again.";
            yield return null;
        }

        _notificationText.text = "Please wait trying to login / register account ...";
        yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
        _usernamePanel.gameObject.SetActive(true);
        _notificationText.gameObject.SetActive(false);
    }

    IEnumerator CheckUsernameExist()
    {
        _notificationText.gameObject.SetActive(true);
        WWWForm usernameForm = new WWWForm();
        usernameForm.AddField("usernamePost", _usernameInputField.text);

        _usernamePanel.gameObject.SetActive(false);
        _notificationText.text = "Please wait trying to login / register account ...";

        WWW www = new WWW(checkUsernameURL, usernameForm);
        float timer = 0;
        float timeOut = 10;
        bool failed = false;
        while (!www.isDone)
        {
            if (timer > timeOut) { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (failed || !string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            _notificationText.text = www.error;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _usernamePanel.gameObject.SetActive(true);
            yield break;
        }

        registeredUsername = _usernameInputField.text;
        if (www.text == checkUsernameSuccessStr) _loginPanel.gameObject.SetActive(true);
        else _registerPanel.gameObject.SetActive(true);
        _notificationText.gameObject.SetActive(false);
    }

    // register
    IEnumerator RegisterUsername()
    {
        _notificationText.gameObject.SetActive(true);
        WWWForm registerForm = new WWWForm();
        registerForm.AddField("usernamePost", registeredUsername);
        registerForm.AddField("passwordPost", _registerPasswordInputField.text);
        registerForm.AddField("recentLevelPost", defaultNextLevelName);

        _registerPanel.gameObject.SetActive(false);
        _notificationText.text = "Please wait trying to register account ...";

        WWW www = new WWW(registerURL, registerForm);
        float timer = 0;
        float timeOut = 10;
        bool failed = false;
        while (!www.isDone)
        {
            if (timer > timeOut) { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (failed || !string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            _notificationText.text = www.error;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _registerPanel.gameObject.SetActive(true);
            yield break;
        }

        yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
        if (www.text == registerSuccessStr)
        {
            PlayerPrefs.SetString("name", registeredUsername);
            PlayerPrefs.SetString("lastLevelName", defaultNextLevelName);
            PlayerPrefs.SetInt("currentPoint", 0);
            nextLevelName = defaultNextLevelName;

            _notificationText.text = registerSuccessStr;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            transform.FindChild("Account Panel").gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            _notificationText.text = www.text;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _registerPanel.gameObject.SetActive(true);
            _notificationText.gameObject.SetActive(false);
        }
    }

    // login
    IEnumerator LoginUsername()
    {
        _notificationText.gameObject.SetActive(true);
        WWWForm loginForm = new WWWForm();
        loginForm.AddField("usernamePost", registeredUsername);
        loginForm.AddField("passwordPost", _loginPasswordInputField.text);

        _loginPanel.gameObject.SetActive(false);
        _notificationText.text = "Please wait trying to login account ...";

        WWW www = new WWW(loginURL, loginForm);
        float timer = 0;
        float timeOut = 10;
        bool failed = false;
        while (!www.isDone)
        {
            if (timer > timeOut) { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (failed || !string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            _notificationText.text = www.error;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _loginPanel.gameObject.SetActive(true);
            yield break;
        }

        yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
        if (www.text == loginUnsuccessfulStr)
        {
            _notificationText.text = www.text;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _loginPanel.gameObject.SetActive(true);
            _notificationText.gameObject.SetActive(false);
        }
        else
        {
            string playerDataString = www.text;
            string[] players = playerDataString.Split(';');
            string username = GetDataValue(players[0], "Username:");
            string recentLevel = GetDataValue(players[0], "Recent_Level:");
            int totalScore = int.Parse(GetDataValue(players[0], "Total_Score:"));

            PlayerPrefs.SetString("name", username);
            PlayerPrefs.SetString("lastLevelName", recentLevel);
            PlayerPrefs.SetInt("currentPoint", totalScore);
            nextLevelName = recentLevel;

            _notificationText.text = "Welcome back, "+username;
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            transform.FindChild("Account Panel").gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }
    
    public void LoginRegisterButton() { StartCoroutine(CheckUsernameExist()); }
    public void RegisterButton() { StartCoroutine(RegisterUsername()); }
    public void LoginButton() { StartCoroutine(LoginUsername()); }
	public void ChangeLevel() {	SceneManager.LoadScene(nextLevelName); }
}

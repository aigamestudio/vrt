﻿using UnityEngine;
using System.Collections;

/**
*MiniGameBehaviour
*Class that will be triggered by player when player approached certain waypoint
*that have a mini game on it. Hence this class is neded if mini game wants to start
*/
public class MiniGameBehaviour : VRButtonBehaviour {
    [HideInInspector] protected GameManagerBehaviour _gameManager;
    [HideInInspector] protected Animator _animator;
    [HideInInspector] protected Transform _hologram;

    protected bool isUnlocked = false;
	[SerializeField] protected int truePoint = 50;
	[SerializeField] protected int falsePoint = -20;

    protected void Awake()
    {
        _gameManager = FindObjectOfType<GameManagerBehaviour>();
        _player = FindObjectOfType<PlayerBehaviour>();
        _animator = GetComponent<Animator>();
        _hologram = transform.FindChild("Hologram");
    }

	public void SetInteractable(bool cond) {
		GetComponent<Collider>().enabled = cond;
		transform.FindChild("Interact Meshes").gameObject.SetActive(cond);
	}

    public virtual Vector3 GenerateHologramCenter()
    {
        return _hologram.position;
    } 

	public bool GetIsUnlocked() { return isUnlocked; }
	public void PlaySound(AudioClip _sfx) { GetComponent<AudioSource>().PlayOneShot(_sfx); }
}

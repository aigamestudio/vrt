﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

/**
*UICognitiveMiniGameBehaviour
*Class that will control's UI of the cognitif mini game behaviour
*This class will be main class that will be control's everything that will be seen by player
*when cognitve mini game is  played
**/
public class UICognitiveMiniGameBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] LevelBehaviour _level;
	[HideInInspector] GameTimeManagerBehaviour _gameTimeManager;
	[HideInInspector] CognitiveMiniGameBehaviour _cognitiveMiniGame;
	[HideInInspector] Text _titleText;
	[HideInInspector] Text _helperText;

	[SerializeField] float alarmStart = 10.0f;
	[SerializeField] string gameOverStr = "Game Over!";
	int lastAlarm;

	[SerializeField] string lockedStr = "Door is Locked.";
	[SerializeField] string hackStr = "Initialize Hacking ...";
	[SerializeField] string unlockedStr = "Door is Unlocked!";

	[SerializeField] string helperQuestionStr = "Remember the possible password list.";
	[SerializeField] string helperAnswerStr = "Choose the correct password from password list.";

	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_gameTimeManager = FindObjectOfType<GameTimeManagerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
		_titleText = transform.FindChild("Title Text").GetComponent<Text>();
		_helperText = transform.FindChild("Helper Text").GetComponent<Text>();
	}

	void OnEnable() {
		_cognitiveMiniGame = transform.GetComponentInParent<CognitiveMiniGameBehaviour>();
		lastAlarm = (int)alarmStart;
	}

	void Update () {
		switch(_gameManager.GetState()) {
			case GameManagerBehaviour.GameState.CognitiveMiniGameAnswer:
				HandleAnswerPhase();
				_helperText.text = helperAnswerStr;
				break;
			case GameManagerBehaviour.GameState.CognitiveMiniGameQuestion:
				_titleText.text = hackStr;
				_helperText.text = helperQuestionStr;
				break;
			default:
				_titleText.text = (_cognitiveMiniGame.GetIsUnlocked()) ? unlockedStr : lockedStr;
				_helperText.text = "";
				break;
		}
	}

	private void HandleAnswerPhase() {
		if(_level.GetLevelInitialTimeLeft() > 0.0f) {
			float timeLeft = _gameTimeManager.GetTimeLeft();
			_titleText.text = (timeLeft > 0.0f) ? ConvertFloatToTime(timeLeft) + "s" : gameOverStr;
			if(timeLeft <= lastAlarm) {
				lastAlarm--;
				if(timeLeft > 0.0f) GetComponent<Animator>().SetTrigger("alert");
				else GetComponent<Animator>().SetTrigger("over");
			}
		}
	}

	private string ConvertFloatToTime(float timeFloat) {
		var ms = (int)((timeFloat - (int)timeFloat) * 10);
		var ss = Convert.ToInt32(timeFloat % 60).ToString("00");
		var mm = (Math.Floor(timeFloat / 60) % 60).ToString("00");
		return mm + ":" + ss + ":" + ms;
	}
		
	public void PlaySound(AudioClip _sfx) { GetComponent<AudioSource>().PlayOneShot(_sfx); }
}

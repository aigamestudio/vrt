﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

/**
*AudioManagerBehaviour
*This class will be handle's the audio that plays during the game
*This class is stores the audio sorce that used in the game
**/
public class AudioManagerBehaviour : MonoBehaviour {
	public static AudioManagerBehaviour instance;
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] AudioSource _audioSource;

	public static AudioManagerBehaviour Instance {
		get { return instance; }
	}

	void Awake() {
		if(instance != null && instance != this) {
			Destroy(this.gameObject);
			return;
		}
		else {
			instance = this;
		}

		DontDestroyOnLoad(this.gameObject);
		StartCoroutine(GetGameManager());
		_audioSource = GetComponent<AudioSource>();
	}

	void Update() {
		if(_gameManager) {
			if(_gameManager.GetState() == GameManagerBehaviour.GameState.Exploring && !_audioSource.isPlaying) {
				_audioSource.Play();
			}
		}
	}

	IEnumerator GetGameManager() {
		while(true) {
			if(!_gameManager) _gameManager = FindObjectOfType<GameManagerBehaviour>();

			yield return null;
		}
	}
}

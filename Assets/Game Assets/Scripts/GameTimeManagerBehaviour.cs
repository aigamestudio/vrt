﻿using UnityEngine;
using System.Collections;

/**
*GameTimeManagerBehaviour
*Class that will control's time limit behaviour
*This class will store's the remaining time and passed time
**/
public class GameTimeManagerBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] LevelBehaviour _level;

	float timePassed = 0.0f;
	float timeLeft = 0.0f;
	
	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
	}

	void OnEnable() {
		timeLeft = _level.GetLevelInitialTimeLeft();
	}
	
	void Update () {
		HandleTimeLeft();

		switch(_gameManager.GetState()) {
			case GameManagerBehaviour.GameState.Moving: UpdateTimePassed(); break;
			case GameManagerBehaviour.GameState.CognitiveMiniGameAnswer: UpdateTimePassed(); break;
		}
	}

	private void HandleTimeLeft() {
		if(_level.GetLevelInitialTimeLeft() > 0.0f) {
			timeLeft = _level.GetLevelInitialTimeLeft() - timePassed;
			bool isLose = timeLeft <= 0.0f;
			if(isLose) _gameManager.LoseState();
		}
	}

	private void UpdateTimePassed() {
		timePassed += Time.deltaTime;
    }
	
	public float GetTimeLeft() { return timeLeft; }
}

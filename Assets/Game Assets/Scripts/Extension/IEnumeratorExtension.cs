﻿using UnityEngine;
using System.Collections;

public static class IEnumeratorExtension {

    public static IEnumerator _WaitForRealSeconds(float aTime)
    {
        while (aTime > 0f)
        {
            aTime -= Mathf.Clamp(Time.unscaledDeltaTime, 0, 0.2f);
            yield return null;
        }
    }

}

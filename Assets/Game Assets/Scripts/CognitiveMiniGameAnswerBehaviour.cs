﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/**
*CognitiveMiniGameAnswerBehaviour
*Class that will control's every mini game answer object behaviour
*This class is attched to  the mini game answer object that's is generated
*when mini play is being played
*/
public class CognitiveMiniGameAnswerBehaviour : VRButtonBehaviour {
	public const int MAX_ANSWER_ID = 20;

	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] Animator _animator;

	[SerializeField] float maxSpeedRotation = 180.0f;
	[HideInInspector] public int answerId = 0;
	bool isOpen = true;

	void Awake() {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_player = FindObjectOfType<PlayerBehaviour>();
		_animator = GetComponent<Animator>();
	}

	void Update() {
		HandleRotation();
		HandleAnimator();
	}

	void OnEnable() {
		transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);
	}

	private void HandleRotation() {
		Vector3 newRotEuler = new Vector3(0.0f, (isOpen) ? 0.0f : 180.0f, 0.0f);
		transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(newRotEuler), maxSpeedRotation * Time.deltaTime);
    }

	private void HandleAnimator() {
		_animator.SetInteger("answerId", answerId);
	}

	public override void ActivatePlayerPointer() {
		if((_gameManager.GetState() == GameManagerBehaviour.GameState.CognitiveMiniGameAnswer || _gameManager.GetState() == GameManagerBehaviour.GameState.PhysicalMiniGameAnswer) && !isOpen) {
			CognitiveMiniGameBehaviour cognitiveMiniGame = (CognitiveMiniGameBehaviour)_player.currentMiniGame;
			cognitiveMiniGame.ProcessAnswerId(transform.GetSiblingIndex());

			_player.GetComponent<Animator>().SetBool("pointer", false);
			isOpen = true;
		}
	}

	public override void SetPlayerPointer(bool cond) {
		if((_gameManager.GetState() == GameManagerBehaviour.GameState.CognitiveMiniGameAnswer || _gameManager.GetState() == GameManagerBehaviour.GameState.PhysicalMiniGameAnswer) && !isOpen) {
			_player.GetComponent<Animator>().SetBool("pointer", cond);
		}
	}

	public void SetIsOpen(bool cond) { isOpen = cond; }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SurveyManagerBehaviour : MonoBehaviour {
    public static SurveyManagerBehaviour Instance;

    [Header("Survey")]
    [SerializeField] GameObject _surveyWindow;
    [SerializeField] GameObject _surveyPanel;
    [SerializeField] Text _questionText;
    [SerializeField] InputField _answerInputField;
    [SerializeField] Text _notificationText;
    [SerializeField] string submitURL = "https://segarbugarindo.com/nis/SurveySubmit.php";
    [SerializeField] string submitSuccessStr = "Submit success";

    string levelName;
    bool isSubmit = false;

    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void OpenSurveyWindow(string question, string currentLevelName) {
        if(Application.internetReachability != NetworkReachability.NotReachable)
        {
            Time.timeScale = 0;
            levelName = currentLevelName;
            _questionText.text = question;
            _surveyWindow.gameObject.SetActive(true);
            _surveyPanel.SetActive(true);
        }
    }

    public void SkipSurveyButton()
    {
        Time.timeScale = 1;
        _surveyWindow.gameObject.SetActive(false);
    }

    public void SubmitSurvey()
    {
        if(!isSubmit)
        {
            _surveyPanel.SetActive(false);
            StartCoroutine(SubmitSurveyCoroutine());
        }
    }

    IEnumerator SubmitSurveyCoroutine()
    {
        isSubmit = true;
        _notificationText.gameObject.SetActive(true);
        while (Application.internetReachability == NetworkReachability.NotReachable)
        {
            if (_notificationText.gameObject.activeSelf)
            {
                _notificationText.text = "Couldn't save to database.\nPlease check your internet connection.";
                yield return IEnumeratorExtension._WaitForRealSeconds(3.0f);
                _notificationText.gameObject.SetActive(false);
            }
            yield return null;
        }

        _notificationText.gameObject.SetActive(true);
        _notificationText.text = "Saving to database ...";
        yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);

        WWWForm updateForm = new WWWForm();
        updateForm.AddField("usernamePost", PlayerPrefs.GetString("name"));
        updateForm.AddField("levelPost", levelName);
        updateForm.AddField("questionPost", _questionText.text);
        updateForm.AddField("answerPost", _answerInputField.text);
        WWW www = new WWW(submitURL, updateForm);
        float timer = 0;
        float timeOut = 10;
        bool failed = false;
        while (!www.isDone)
        {
            if (timer > timeOut) { failed = true; break; }
            timer += Time.deltaTime;
            yield return null;
        }
        if (failed || !string.IsNullOrEmpty(www.error))
        {
            www.Dispose();
            _notificationText.text = "Unable to connect to database";
            yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
            _notificationText.gameObject.SetActive(false);
            yield break;
        }

        if (www.text == submitSuccessStr) _notificationText.text = submitSuccessStr;
        else _notificationText.text = "Unable to connect to database";

        yield return IEnumeratorExtension._WaitForRealSeconds(1.0f);
        _notificationText.gameObject.SetActive(false);
        _surveyWindow.SetActive(false);
        Time.timeScale = 1;
        isSubmit = false;
    }
}

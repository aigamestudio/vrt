﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VRButtonBehaviour : MonoBehaviour {
	[HideInInspector] protected PlayerBehaviour _player;

	[SerializeField] MonoBehaviour _defaultClassCall;
	[SerializeField] string _defaultMethodCallName;

	void Awake() {
		_player = FindObjectOfType<PlayerBehaviour>();
	}

	public virtual void ActivatePlayerPointer() {
		if(_defaultClassCall) {
			_player.GetComponent<Animator>().SetBool("pointer", false);
			_defaultClassCall.Invoke(_defaultMethodCallName, 0.0f);
		}
	}

	public virtual void SetPlayerPointer(bool cond) {
		_player.GetComponent<Animator>().SetBool("pointer", cond);
		//if(cond) Invoke("ActivatePlayerPointer", 1.0f); else CancelInvoke();
	}
}

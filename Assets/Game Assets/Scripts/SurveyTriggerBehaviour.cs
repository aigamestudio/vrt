﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SurveyTriggerBehaviour : MonoBehaviour {
    [SerializeField] [TextArea(1, 3)] string question = "Question";

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerBehaviour>())
        {
            SurveyManagerBehaviour.Instance.OpenSurveyWindow(question, SceneManager.GetActiveScene().name);
            Destroy(gameObject);
        }
    }
}

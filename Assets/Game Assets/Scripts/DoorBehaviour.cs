﻿using UnityEngine;
using System.Collections;

/**
*DoorBehaviour
*Class that will control's door behaviour
*This class will determine whether the player can pass or not
*/
public class DoorBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] PlayerBehaviour _player;
	[HideInInspector] Animator _animator;
	[HideInInspector] Collider _collider;

	[SerializeField] MiniGameBehaviour[] _unlockedMiniGames;
	[SerializeField] float doorRadius = 15.0f;

	void Awake () {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_player = FindObjectOfType<PlayerBehaviour>();
		_animator = GetComponent<Animator>();
		_collider = GetComponent<Collider>();
	}
	
	void Update () {
		bool checkIsUnlocked = CheckIsUnlocked();
		_collider.enabled = !checkIsUnlocked;
		_animator.SetBool("isOpen", checkIsUnlocked && CheckIsOpen());
	}

	private bool CheckIsUnlocked() {
		for(int i=0; i<_unlockedMiniGames.Length; i++) {
			if(!_unlockedMiniGames[i].GetIsUnlocked()) return false;
		}

		return true;
	}

	private bool CheckIsOpen() {
		bool isInState = _gameManager.GetState() != GameManagerBehaviour.GameState.Intro 
							&& _gameManager.GetState() != GameManagerBehaviour.GameState.Win
							&& _gameManager.GetState() != GameManagerBehaviour.GameState.CognitiveMiniGameQuestion
                            && _gameManager.GetState() != GameManagerBehaviour.GameState.CognitiveMiniGameAnswer
                            && _gameManager.GetState() != GameManagerBehaviour.GameState.PhysicalMiniGameQuestion
                            && _gameManager.GetState() != GameManagerBehaviour.GameState.PhysicalMiniGamePlay
                            && _gameManager.GetState() != GameManagerBehaviour.GameState.PhysicalMiniGameAnswer;
		bool isInDistance = Vector3.Distance(transform.position, _player.transform.position) < doorRadius;
		return isInState && isInDistance;
	}

	private void PlaySound(AudioClip sfx) {
		GetComponent<AudioSource>().PlayOneShot(sfx);
	} 
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

/**
*UILiftExitBehaviour
*Control's everything that happens from when the player arrived at the end of the maze
*and enter the lift such as decide which level will be executed after current level
**/
public class UILiftExitBehaviour : MonoBehaviour {
	[HideInInspector] GameManagerBehaviour _gameManager;
	[HideInInspector] GameTimeManagerBehaviour _gameTimeManager;
	[HideInInspector] LevelBehaviour _level;
	[HideInInspector] Text _levelNameText;
	[HideInInspector] Text _timeLeftText;
	[HideInInspector] Text _timeUsedText;
	[HideInInspector] Text _nameText;
	[HideInInspector] Text _totalTrueAnswerText;
	[HideInInspector] Text _totalFalseAnswerText;
	[HideInInspector] Text _pointText;

	[SerializeField][Range(0, 100)] float minTimeBonusPoint = 0.3f;
	[SerializeField] int bonusPoint = 100;
	[SerializeField] int defaultPoint = 50;

	[SerializeField] float alarmStart = 10.0f;
	[SerializeField] string gameOverStr = "Game Over!";
	int lastAlarm;

	[SerializeField] AudioClip _winSound;
	bool isWin = false;

	string playerName = "";

	void Awake () {
		_gameManager = FindObjectOfType<GameManagerBehaviour>();
		_gameTimeManager = FindObjectOfType<GameTimeManagerBehaviour>();
		_level = FindObjectOfType<LevelBehaviour>();
		_levelNameText = transform.FindChild("Level Name Text").GetComponent<Text>();
		_timeLeftText = transform.FindChild("Time Left Text").GetComponent<Text>();
		_timeUsedText = transform.FindChild("Time Used Text").GetComponent<Text>();
		_nameText = transform.FindChild("Name Text").GetComponent<Text>();
		_totalTrueAnswerText = transform.FindChild("Total True Answer Text").GetComponent<Text>();
		_totalFalseAnswerText = transform.FindChild("Total False Answer Text").GetComponent<Text>();
		_pointText = transform.FindChild("Point Text").GetComponent<Text>();
	}

	void OnEnable() {
		lastAlarm = (int)alarmStart;
		playerName = PlayerPrefs.GetString("name", "");

		_timeLeftText.gameObject.SetActive(_level.GetLevelInitialTimeLeft() > 0.0f);
    }
	
	void Update () {
		_levelNameText.text = _level.GetLevelName();
		_timeLeftText.text = _gameTimeManager.GetTimeLeft().ToString();
		_timeUsedText.text = (_level.GetLevelInitialTimeLeft() - _gameTimeManager.GetTimeLeft()).ToString();
		_nameText.text = playerName;
		_totalTrueAnswerText.text = _gameManager.trueAnswerCount + " answer";
		_totalFalseAnswerText.text = _gameManager.falseAnswerCount + " answer";
		_pointText.text = _gameManager.currentPoint + " point";

		bool getBonusPoint = _gameTimeManager.GetTimeLeft() / _level.GetLevelInitialTimeLeft() > minTimeBonusPoint;

		if(_level.GetLevelInitialTimeLeft() > 0.0f) {
			float timeLeft = _gameTimeManager.GetTimeLeft();
			float timeUsed = _level.GetLevelInitialTimeLeft() - _gameTimeManager.GetTimeLeft();
			string bonusStr = (getBonusPoint) ? " (+" + bonusPoint + " points)" : " (+" + defaultPoint + " points)";
			_timeLeftText.text = (timeLeft > 0.0f) ? ConvertFloatToTime(timeLeft) + "s" : gameOverStr;
			_timeUsedText.text = (timeLeft > 0.0f) ? ConvertFloatToTime(timeUsed) + "s" + bonusStr : gameOverStr;
			if(timeLeft <= lastAlarm) {
				lastAlarm--;
				if(timeLeft > 0.0f) GetComponent<Animator>().SetTrigger("alert");
				else GetComponent<Animator>().SetTrigger("over");
			}
		}

		if(_gameManager.GetState() == GameManagerBehaviour.GameState.Win && !isWin) {
			isWin = true;
			PlaySound(_winSound);
			if(_level.GetLevelInitialTimeLeft() > 0.0f) {
				_gameManager.currentPoint += (getBonusPoint) ? bonusPoint : defaultPoint;
			}
		}
	}

	private string ConvertFloatToTime(float timeFloat) {
		var ms = (int)((timeFloat - (int)timeFloat) * 10);
		var ss = Convert.ToInt32(timeFloat % 60).ToString("00");
		var mm = (Math.Floor(timeFloat / 60) % 60).ToString("00");
		return mm + ":" + ss + ":" + ms;
	}
		
	public void PlaySound(AudioClip _sfx) {
		GetComponent<AudioSource>().PlayOneShot(_sfx);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

/**
*UILiftIntroBehaviour
*Control's everything that happens when the player in the starting lift
*such as the story text until the things that happend when the player leave the lift
**/
public class UILiftIntroBehaviour : MonoBehaviour {
	[HideInInspector] GameTimeManagerBehaviour _gameTimeManager;
	[HideInInspector] AudioSource _audio;
	[HideInInspector] Text _subtitleText;
	[HideInInspector] Image _storyBackgroundImage;
	[HideInInspector] Text _timeLeftText;
	[HideInInspector] LevelBehaviour _level;

	[SerializeField] float alarmStart = 10.0f;
	[SerializeField] string gameOverStr = "Game Over!";
	int lastAlarm;

	[SerializeField] float storySpeed = 0.25f;
	[SerializeField] Story[] stories;
	int storyCount = 0;

	[System.Serializable]
	public class Story : System.Object {
		public AudioClip storyAudio;
		public Sprite sprite;
		[TextArea(1,3)]public string storyDialogue;
	}

	void Awake () {
		_gameTimeManager = FindObjectOfType<GameTimeManagerBehaviour>();
		_timeLeftText = transform.FindChild("Time Left Text").GetComponent<Text>();
		_level = FindObjectOfType<LevelBehaviour>();

		_audio = GetComponent<AudioSource>();
		_subtitleText = transform.FindChild("Subtitle Text").GetComponent<Text>();
		_storyBackgroundImage = transform.FindChild("Story Background").GetComponent<Image>();
	}
	
	void OnEnable () {
		lastAlarm = (int)alarmStart;

		_audio.PlayOneShot(stories[0].storyAudio);
		_storyBackgroundImage.sprite = stories[0].sprite;
		StartCoroutine(ShowWithAnimateText(_subtitleText, stories[0].storyDialogue, storySpeed));
	}

	void Update () {
		if(!_audio.isPlaying) {
			if(storyCount + 1 < stories.Length) {
				StopAllCoroutines();

				storyCount++;
				_audio.PlayOneShot(stories[storyCount].storyAudio);
				_storyBackgroundImage.sprite = stories[storyCount].sprite;
				StartCoroutine(ShowWithAnimateText(_subtitleText, stories[storyCount].storyDialogue, storySpeed));
			}
			else { 
				_subtitleText.text = "";
				_storyBackgroundImage.gameObject.SetActive(!(_level.GetLevelInitialTimeLeft() > 0.0f));
				_timeLeftText.gameObject.SetActive(_level.GetLevelInitialTimeLeft() > 0.0f);

				if(_level.GetLevelInitialTimeLeft() > 0.0f) {
					float timeLeft = _gameTimeManager.GetTimeLeft();
					_timeLeftText.text = (timeLeft > 0.0f) ? ConvertFloatToTime(timeLeft) + "s" : gameOverStr;
					if(timeLeft <= lastAlarm) {
						lastAlarm--;
						if(timeLeft > 0.0f) GetComponent<Animator>().SetTrigger("alert");
						else GetComponent<Animator>().SetTrigger("over");
					}
				}
			}
		}
	}

	private IEnumerator ShowWithAnimateText(Text text, string strComplete, float speed) {
		int i = 0;
		string str = "";
		while(i < strComplete.Length) {
			str += strComplete[i++];
			text.text = str;
			yield return new WaitForSeconds(speed * Time.deltaTime);
		}
	}

	private string ConvertFloatToTime(float timeFloat) {
		var ms = (int)((timeFloat - (int)timeFloat) * 10);
		var ss = Convert.ToInt32(timeFloat % 60).ToString("00");
		var mm = (Math.Floor(timeFloat / 60) % 60).ToString("00");
		return mm + ":" + ss + ":" + ms;
	}

	public void PlaySound(AudioClip _sfx) {
		GetComponent<AudioSource>().PlayOneShot(_sfx);
	}
}
